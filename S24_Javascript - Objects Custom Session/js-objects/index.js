console.log("Recommended Books");

let recommendedBookOne = {
	author: "Anne Rice",
	title: "The Witching Hour",
	published: "1990",
	summary: "Lives of the Mayfair Witches is a trilogy of Gothic supernatural horror/fantasy novels by American novelist Anne Rice. It centers on a family of witches whose fortunes have been guided for generations by a spirit named Lasher."
}

let recommendedBookTwo = {
	author: "Anne Rice",
	title: "Lasher",
	published: "1993",
	summary: "The beautiful Rowan Mayfair, queen of the coven, must flee from the darkly brutal, yet irresidtible demon known as Lasher."
}

let recommendedBookThree = {
	author: "Anne Rice",
	title: "Taltos",
	published: "1994",
	summary: "Taltos takes readers back through the centuries to a civilization part human and part of wholly mysterious origins, at odds with mortality and immortality, justice and guilt."
}

console.log(recommendedBookOne);
console.log(recommendedBookTwo);
console.log(recommendedBookThree);


