console.log("Characters");

let dafi = {
	name: "Dafi",
	role: "Enchanter",
	level: 12,
	health: 2.5*this.level,
	attack: (2.5*this.level)/1.5,
	healing: 1.5*this.level,
	talk: function(){
		console.log("Let me keep you safe")
	},
	bolt: function(target){
		console.log("hark by the might of the heavens let this crashing light tear and destroy lightning bolt")
		target.health -= this.attack;
		console.log(target.name + " health is now reduced to " + target.health)
		if (target.health <= 0) {
			console.log(target.name + ' has fainted.')
		}
	},
	heal: function(target){
		console.log("Come forth, o aurora light. Apareo Spheralloon!")
		target.health += this.healing;
		console.log(target.name + " health is now " + target.health)
		if (target.health <= 0) {
			console.log(target.name + ' has fainted.')
		}
	},
	faint: function(){
		console.log(this.name+` fainted`)
	}
}

let cloudy = {
	name: "Cloudy",
	role: "Hunter",
	level:  15,
	health: 2.5*this.level,
	attack: (1.5*this.level),
	talk: function(){
		console.log("hmmm");
	},
	arrow: function(target){
		target.health -= this.attack;
		console.log(target.name + " health is now reduced to " + target.health)
		if (target.health <= 0) {
			console.log(target.name + ' has fainted.')
		}	
	},
	assasinate: function(target){
		this.attack * 100000
		target.health -= this.attack;
	},
	faint: function(){
		console.log(this.name+` fainted`)
	}
}

let rocco = {
	name: "Rocco",
	role: "Warrior",
	level:  16,
	health: 2.5*this.level,
	attack: (2.5*this.level),
	talk: function(){
		console.log("Leave it to me!");
	},
	roar: function(target){
		console.log("RAAAAAWWRRRRRRR")
		target.attack * 3;
		console.log(target.name + " attack is now " + target.attack)
	},
	mighty: function(target){
		console.log("By the power of gray skull! Hiyaaaaaa")
		this.attack * 5;
		target.health -= this.attack;
		console.log(target.name + " health is now reduced to " + target.health)
		if (target.health <= 0) {
			console.log(target.name + ' has fainted.')
		}
	},
	attack: function(target){
		target.health -= this.attack;
		console.log(target.name + " health is now reduced to " + target.health)
		if (target.health <= 0) {
			console.log(target.name + ' has fainted.')
		}
	},
	faint: function(){
		console.log(this.name+` fainted`)
	}
}

console.log(dafi);
console.log(rocco);
console.log(cloudy);

dafi.heal(cloudy);
rocco.mighty(dafi);



