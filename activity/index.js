console.log("JS Activity 24 - ES6 Updates")

let num = 2
const getCube = num ** 3
console.log(`The cube of ${num} is ${getCube}`);

const Address = [2822, 'Liwayway St', 'Bruger Subdivision', 'Putatan', 'Muntinlupa', 1772]
const [StreetNumber, StreetName, DistrictVillage, Barangay, CityMunicipality, PostalCode ] = Address
console.log(`I live at ${StreetNumber} ${StreetName}, ${DistrictVillage}, ${Barangay}, ${CityMunicipality} ${PostalCode}`)

const lolong = {
	name: 'Lolong',
	family: 'saltwater crocodile',
	weight: 1025,
	measurement: "20 ft 3 in"
}
const { name, family, weight, measurement } = lolong;
console.log ( `${name} is a ${family}. He weighed at ${weight} kgs with a measurement of ${measurement}.`)

let numArray = [ 1, 2, 3, 4, 5 ]
numArray.forEach((numArray) => console.log(`${numArray}`));

let reduceNumber = numArray.reduce(function(a, b){
	return a+b
})
console.log(reduceNumber);

class Dog {
	constructor (name, age, breed ){
		this.name = name;
		this.age = age; 
		this.breed = breed
	}
}

const Pet1 = new Dog ("Daffi", 1.5, "Bichon Frise")
console.log(Pet1);

const Pet2 = new Dog ("Cloudy", 10, "Spitz mixed breed")
console.log(Pet2);