console.log("Hello JS - ES6 Updates");

// ES in ES6 means ECMAScript aka ECMAScript 2015 (6th version)
/* ECMAScript - is the standard that is used to create implementations of the language */

// 					SECTION - Exponent Operator
/*
					pre-ES6

					let/const variableName = Math.pow ( number, exponent )
*/
let firstNum = Math.pow(8,2); 
console.log(firstNum);
/*
					post-ES6

					let/const variableName = number ** exponent
*/
let secondNum = 8 ** 2; 
console.log(secondNum);

// 					SECTION - Template Literals
let name = "John";
//					pre-ES6
let message = "Hello " + name + "! Welcome to programming!"
console.log('Message without template literals: ' + message);

//					post-ES6
// single-line
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message without template literals: ${message}`);
// multiple-lines
const anotherMessage = `
${name} won the math competition. 
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}.
He won it by solving the problem 8 ** 2 with the solution of ${8 ** 2}.
`
console.log(anotherMessage);

/*
	Template literals allow us to write strings with embedded JS expressions
	expressions is general are any valid unit of code that resolves into a value
	"${ }" are used to include JS expression in strings using template literals
*/

let interestRate = .15;
let principal = 1000;
let totalInterest = `
Current Balance ${principal}
with interest of ${interestRate * 100}%
Total Interest = ${principal*interestRate}`;
console.log(totalInterest);
console.log(`The total interest on your savings account is: ${interestRate * principal}`);

// 					SECTION - Array Destructuring
/*
	allows us to unpack elements in arrays to distinct variables
	allows us to name array elements with variables instead of using index numbers
	helps with code readability

					SYNTAX
						let/const [ variableA, variableB, variableC, ... ] = arrayName

						NOTE: we have to be mindful of the variableName and make sure that it describes the right element that is stored inside it
*/
const fullName = [ 'Juan', "Dela", "Cruz", "something" ];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

const [ firstName, middleName, lastName, moreName ] = fullName;
console.log(firstName); 
console.log(middleName); 
console.log(lastName); 
console.log(moreName);
console.log(`Hello ${firstName} ${middleName} ${lastName} ${moreName}!`);

let person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}
console.log(person.givenName); // firstName - returns error due to there is already a similar variable declared
console.log(person.maidenName); // middleName - returns error due to there is already a similar variable declared
console.log(person.familyName); // lastName - returns error due to there is already a similar variable declared
console.log(`Hello ${person.givenName} ${person.maidenName} ${[person.familyName]}!`)

// 					SECTION - Object Destructuring
/*
	allows us to unpack properties of objects into distinct variables
	shortens the syntax for accessing the properties from an object

					SYNTAX
						let/const { propertyA, propertyB, propertyC } = objectName; 

						NOTE: will return error if the dev did not use the correct propertyName
*/

const { givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);

// using object destructuring as a parameter of a function
function getFullName({ givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person);

let pet = {
	petName: "Arrowana",
	trick: "splash",
	treat: "ipis"
} ;

function getPet({ petName, trick, treat }){
	console.log(`${petName}, ${trick}!`)
	console.log(`good ${trick}!`)
	console.log(`here is an ${treat} for you!`);
}
getPet(pet);

// 					SECTION - Arrow Function
/*
	compact alternative syntax to traditional functions
*/
/*const hello = () => {
	console.log("Hello World");
};
*/
// 					pre-ES6
/*function printFullName (firstName, middleName, lastName) {
	console.log(firstName+" "+middleName+" "+lastName)
}
console.log('Portgas','D','Ace')*/

// 					post-ES6 - Arrow function
/*
					SYNTAX
						let/const variableName = ( parameterA, parameterB, parameterC ) => {
							console.log(); /statements/expressions
						}
*/
const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`)
}
printFullName("Portgas", "D.", "Ace");
printFullName("Naruto", "U.", "Shippudden");

// 					Arrow function with loops
// 					pre-ES6

const  students = ["John", "Jane", "Judy"];
students.forEach(function(student){
	console.log(`${student} is a student`)
})

/*const add = (x, y) => {
	return x+y;
}
console.log(add(5,5));*/

//					post-ES6 - Arrow function with implicit return				
/*
	there are instances where we can omit the "return" statement;
	this works because even without the return statement, JS can implicitly adds it for the result of the function

*/
const add = ( x, y ) => x + y;
console.log(add(99,888));

let addVariable = add(99,8888);
console.log(addVariable);

// 					SECTION - Default Argument Value
const greet = (name = "User", age = "unknown") => {
	return `Good Morning ${name} ${age}!`
}
console.log(greet("Marco", 19));
/*
	the "name = "User" sets the default value for the function greet() once it is called without any parameters
	no value - would return "Good Morning User" because the function is called without any arguments
		console.log(greet());
*/

// 					SECTION - Class-object Object BluePrints
/*
	allows creation / instantiation of objects using classes as blueprints

	creating a class
		constructor is a special method of a class for creating / initializing an object for that class
		"this." keyword refers to the properties of an object created from the class; this allows us to reassign values for the properties inside the class
		creating a class is another way to create objects in JS.

					SYNTAX
						class className {
							constructor ( objectPropertyA, objectPropertyB, ....){
								this.objectPropertyA = objectPropertyA;
								this.objectPropertyB = objectPropertyB;
							}
						}
*/

class Car{
	constructor (brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
};

/*
	first instance
		using initializer and/or dot notation, create a car object using the Car class that we have created

		"new" creates new object with the given arguments as values of its properties
		no arguments provided will create an object without any values assigned to it, meaning the properties would return "undefined"

					SYNTAX
						let/const variableName = new ClassName();
					
						const myCar = new Car();

	creating with const keyword and assigning a value of an object makes it so we cannot re-assign another data type i.e. array
	it does not mean that its properties cannot be changed or manipulated

*/

	const myCar = new Car();
	myCar.brand = 'Jaguar Land Rover';
	myCar.name = 'Range Rover Vogue';
	myCar.year = 1984;
	console.log(myCar);
// second instance
	const myNewCar = new Car("Toyota", "Vios", 2021);
	console.log(myNewCar);


